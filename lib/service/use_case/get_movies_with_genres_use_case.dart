import 'package:movie_database/model/genre.dart';
import 'package:movie_database/model/movie.dart';
import 'package:movie_database/service/repository/genres_repository.dart';
import 'package:movie_database/service/repository/movie_repository.dart';

class GetMoviesWithGenresUseCase {
  final GenresRepository genresRepository;
  final MovieRepository movieRepository;

  GetMoviesWithGenresUseCase(this.genresRepository, this.movieRepository);

  Future<List<Movie>> createRequest(int page) async {
    var genres = await genresRepository.getGenres();
    var moviesWithGenreId = await movieRepository.getPopularMovies(page);

    return moviesWithGenreId
        .map((movie) => Movie.withGenres(
            movie,
            movie.genres
                .map((genre) => Genre(genre.id, genres[genre.id]))
                .toList()))
        .toList();
  }
}
