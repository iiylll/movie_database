import 'dart:math';

import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:movie_database/constant/service_constant.dart';
import 'package:movie_database/di/injector.dart';
import 'package:movie_database/model/genre.dart';
import 'package:movie_database/model/movie.dart';
import 'package:movie_database/util/service_utils.dart';

//TODO#2:1h:count that constants from the first request and store it.
const RESULT_PER_PAGE = 20;
const TOTAL_RESULTS = 10000;
const TOTAL_PAGES = 500;

abstract class MovieRepository {
  Future<List<Movie>> getPopularMovies(int page);
  Future<List<Movie>> search(String query, int page);
}

class ProdMovieRepository extends MovieRepository {
  @override
  Future<List<Movie>> getPopularMovies(int page) async {
    if (page > TOTAL_PAGES) {
      throw NoNextPageException();
    }
    var apiKey = Injector().apiKey;
    try {
      var response = await Injector().dioClient.get(
          '$MOVIE_API_BASE_URL/movie/popular?sort_by=popularity.desc&page=$page&api_key=$apiKey',
          options: buildCacheOptions(Duration(days: 1),
              maxStale: Duration(days: 5)));
      var listOfMovie =
          List<Map<String, dynamic>>.from(response.data['results']);
      return List.generate(
          listOfMovie.length, (index) => Movie.fromJson(listOfMovie[index]));
    } on DioError catch (error) {
      throw Exception(handleError(error));
    } catch (e) {
      throw Exception(UNKNOWN_ERROR_MESSAGE);
    }
  }

  @override
  Future<List<Movie>> search(String query, int page) async {
    var apiKey = Injector().apiKey;
    try {
      var response = await Injector().dioClient.get(
          '$MOVIE_API_BASE_URL/search/movie?query=$query&page=$page&api_key=$apiKey',
          options: buildCacheOptions(Duration(days: 1),
              maxStale: Duration(days: 5)));
      var listOfMovie =
          List<Map<String, dynamic>>.from(response.data['results']);
      return List.generate(
          listOfMovie.length, (index) => Movie.fromJson(listOfMovie[index]));
    } on DioError catch (error) {
      throw Exception(handleError(error));
    } catch (e) {
      throw Exception(UNKNOWN_ERROR_MESSAGE);
    }
  }
}

class MockMovieRepository extends MovieRepository {
  String _fakeDescription =
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'
      ' Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam '
      'ultricies odio, vitae placerat pede sem sit amet enim. Duis risus. '
      'Nunc auctor. Etiam neque. Vivamus porttitor turpis ac leo. Mauris metus. '
      'Integer malesuada. Proin pede metus, vulputate nec, fermentum fringilla, '
      'vehicula vitae, justo. Nulla est. Nullam faucibus mi quis velit. '
      'Praesent dapibus.'
      'Nulla non arcu lacinia neque faucibus fringilla. Fusce suscipit libero '
      'eget elit. In dapibus augue non sapien. Vivamus luctus egestas leo. '
      'Etiam egestas wisi a erat. Phasellus et lorem id felis nonummy placerat.'
      ' Praesent id justo in neque elementum ultrices. Aliquam ornare wisi eu'
      ' metus. Suspendisse nisl. Nulla pulvinar eleifend sem. Donec quis nibh'
      ' at felis congue commodo.';

  @override
  Future<List<Movie>> getPopularMovies(int page) async {
    return Future.delayed(
        Duration(milliseconds: 300),
        () => List.generate(RESULT_PER_PAGE, (index) {
              var random = Random.secure();
              return Movie(_fakeDescription, random.nextDouble(), "",
                  "defaultTitle", [Genre(1, "Action")]);
            }));
  }

  @override
  Future<List<Movie>> search(String query, int page) async {
    return getPopularMovies(page);
  }
}

class NoNextPageException implements Exception {}
