import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:movie_database/constant/service_constant.dart';
import 'package:movie_database/di/injector.dart';
import 'package:movie_database/util/service_utils.dart';

abstract class GenresRepository {
  Future<Map<int, String>> getGenres();
}

class ProdGenresRepository extends GenresRepository {
  @override
  Future<Map<int, String>> getGenres() async {
    Map<int, String> genres = {};
    var apiKey = Injector().apiKey;
    try {
      var response = await Injector().dioClient.get(
          '$MOVIE_API_BASE_URL/genre/movie/list?api_key=$apiKey',
          options: buildCacheOptions(Duration(days: 1),
              maxStale: Duration(days: 5)));
      response.data['genres']
          .forEach((genre) => genres[genre['id']] = genre['name']);
      return genres;
    } on DioError catch (error) {
      throw Exception(handleError(error));
    } catch (e) {
      throw Exception(UNKNOWN_ERROR_MESSAGE);
    }
  }
}

class MockGenresRepository extends GenresRepository {
  @override
  Future<Map<int, String>> getGenres() {
    return Future.value({5: 'actions'});
  }
}
