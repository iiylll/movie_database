import 'package:dio/dio.dart';

String handleError(DioError error) {
  String errorDescription = "";
  switch (error.type) {
    case DioErrorType.CANCEL:
      errorDescription = "Request to API server was cancelled";
      break;
    case DioErrorType.CONNECT_TIMEOUT:
      errorDescription = 'No internet connection';
      break;
    case DioErrorType.DEFAULT:
      errorDescription = 'No internet connection';
      break;
    default:
      errorDescription = 'Server error. Please retry later';
      break;
  }
  return errorDescription;
}
