import 'package:flutter_screenutil/flutter_screenutil.dart';

double setHeight(double value) {
  return ScreenUtil().setHeight(value);
}

double setWidth(double value) {
  return ScreenUtil().setWidth(value);
}

double setSp(double value) {
  return ScreenUtil().setSp(value);
}
