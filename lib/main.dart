import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movie_database/bloc/simple_bloc_observer.dart';
import 'package:movie_database/constant/view_constant.dart';
import 'package:movie_database/di/injector.dart';
import 'package:movie_database/ui/movie_list_page.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  Injector.configure(Flavor.PRODUCTION);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //TODO#3:1h move every style to the theme.
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: RootPage(),
    );
  }
}

class RootPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        designSize: Size(BASE_WIDTH, BASE_HEIGHT), allowFontScaling: false);
    return MovieListPage();
  }
}
