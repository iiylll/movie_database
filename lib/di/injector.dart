import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:movie_database/service/repository/genres_repository.dart';
import 'package:movie_database/service/repository/movie_repository.dart';
import 'package:movie_database/service/use_case/get_movies_with_genres_use_case.dart';

enum Flavor { DEBUG, PRODUCTION }

class Injector {
  static final Injector _instance = Injector._private();
  static Flavor _flavor;
  static Dio _dioClient;

  Injector._private();

  static void configure(Flavor flavor) {
    _flavor = flavor;
    _configureDio();
  }

  static _configureDio() {
    _dioClient = Dio(BaseOptions(
        receiveDataWhenStatusError: true,
        connectTimeout: 10 * 1000, // 60 seconds
        receiveTimeout: 10 * 1000 // 60 seconds
        ));
    _dioClient.interceptors.add(
        InterceptorsWrapper(onRequest: (RequestOptions requestOptions) async {
      final toPrint = {
        'path': requestOptions.path,
        'headerParams': requestOptions.headers.toString(),
        'queryParams': requestOptions.queryParameters.toString(),
        'data': requestOptions.data
      };
      debugPrint(DateTime.now().toIso8601String() +
          "- DIO REQUEST: " +
          toPrint.toString());
      return requestOptions;
    }, onResponse: (Response response) async {
      final toPrint = {
        'path': response.request.path,
        'status': response.statusMessage,
        'data': response.data
      };
      debugPrint(DateTime.now().toIso8601String() +
          "- DIO RESPONSE: " +
          toPrint.toString());
      return response;
    }, onError: (DioError e) async {
      return e;
    }));
  }

  factory Injector() {
    return _instance;
  }

  Dio get dioClient {
    return _dioClient;
  }

  String get apiKey {
    //TODO#1:30m:load api key from property file;
    return '4ff9d08260ed338797caa272d7df35dd';
  }

  MovieRepository get movieRepository {
    return _flavor == Flavor.DEBUG
        ? MockMovieRepository()
        : ProdMovieRepository();
  }

  GenresRepository get genresRepository {
    return _flavor == Flavor.DEBUG
        ? MockGenresRepository()
        : ProdGenresRepository();
  }

  GetMoviesWithGenresUseCase get getMoviesWithGenresUseCase {
    return GetMoviesWithGenresUseCase(genresRepository, movieRepository);
  }
}
