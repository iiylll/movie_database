class Genre {
  final int id;
  String name;

  Genre(this.id, this.name);

  Genre.partly(this.id);

  bool isValid() => name != null;
}
