import 'package:movie_database/constant/model_constant.dart';
import 'package:movie_database/model/genre.dart';

class Movie {
  final String overview;
  final double voteAverage;
  final String posterPath;
  final String title;
  final List<Genre> genres;

  String get originalPosterPath =>
      posterPath == null ? null : MOVIE_IMAGE_BASE_URL + posterPath;

  String get smallPosterPath => MOVIE_IMAGE_SMALL_BASE_URL + posterPath;

  Movie.fromJson(Map<String, dynamic> json)
      : overview = json['overview'] as String,
        voteAverage = (json['vote_average'] as num)?.toDouble(),
        posterPath = json['poster_path'],
        title = json['title'] as String,
        genres = json['genre_ids'] == null
            ? []
            : List<int>.from(json['genre_ids'])
                .map((genreId) => Genre.partly(genreId))
                .toList();

  Movie.withGenres(Movie movie, List<Genre> genres)
      : overview = movie.overview,
        voteAverage = movie.voteAverage,
        posterPath = movie.posterPath,
        title = movie.title,
        genres = genres;

  Movie(this.overview, this.voteAverage, this.posterPath, this.title,
      this.genres);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Movie &&
          runtimeType == other.runtimeType &&
          overview == other.overview &&
          voteAverage == other.voteAverage &&
          posterPath == other.posterPath &&
          title == other.title;

  @override
  int get hashCode =>
      overview.hashCode ^
      voteAverage.hashCode ^
      posterPath.hashCode ^
      title.hashCode;
}
