const MOVIE_IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/w500';
const MOVIE_IMAGE_SMALL_BASE_URL = 'https://image.tmdb.org/t/p/w185';
