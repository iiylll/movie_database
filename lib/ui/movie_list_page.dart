import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_database/bloc/movie_list/movie_list_bloc.dart';
import 'package:movie_database/bloc/movie_list/movie_list_event.dart';
import 'package:movie_database/bloc/movie_list/movie_list_state.dart';
import 'package:movie_database/di/injector.dart';
import 'package:movie_database/model/movie.dart';
import 'package:movie_database/ui/movie_details_page.dart';
import 'package:movie_database/util/view_util.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class MovieListPage extends StatefulWidget {
  @override
  _MovieListPageState createState() => _MovieListPageState();
}

class _MovieListPageState extends State<MovieListPage> {
  MovieListBloc _bloc;
  ScrollController _scrollController;
  //TODO#4:1h:that _loading variable should be removed from this page, added to slow down requests for new pages
  bool _loading;
  int _page;

  @override
  void initState() {
    super.initState();
    _page = 1;
    _loading = false;
    _scrollController = ScrollController();
    _bloc = MovieListBloc(Injector().getMoviesWithGenresUseCase);
    _bloc.add(FetchNextPage(_page));
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  bool _handleScrollNotification(ScrollNotification notification) {
    if (_loading == false && _scrollController.position.extentAfter == 0) {
      _bloc.add(FetchNextPage(_page));
      _loading = true;
    }
    return false;
  }

  int _calculateListItemCount(MovieListState state) {
    if (state.hasReachEndOfResults) {
      return state.movies.length;
    } else {
      return state.movies.length + 1;
    }
  }

  Widget _buildMovieCard(Movie movie) {
    Color _ratingColor;
    if (movie.voteAverage >= 7) {
      _ratingColor = Colors.green;
    } else if (movie.voteAverage >= 3) {
      _ratingColor = Colors.orange;
    } else if (movie.voteAverage > 0) {
      _ratingColor = Colors.red;
    }
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5.0),
      child: InkWell(
        onTap: () {
          //TODO#6:1h:move navigation to separate bloc
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => MovieDetailsPage(movie)));
        },
        child: Card(
            shadowColor: Color(0x30707070),
            elevation: 2,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              height: setHeight(125),
              child: Row(
                children: [
                  Container(
                    height: setHeight(125),
                    margin: EdgeInsets.only(right: 10),
                    child: Hero(
                      tag: movie.originalPosterPath,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: CachedNetworkImage(
                          placeholder: (context, url) => Container(
                              height: setHeight(50), width: setWidth(50)),
                          imageUrl: movie.originalPosterPath,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            child: Text(
                          movie.title,
                          maxLines: 1,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: setSp(15.0),
                          ),
                        )),
                        Container(
                            child: Text(
                          movie.genres.first.name,
                          maxLines: 1,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: setSp(15.0),
                          ),
                        )),
                      ],
                    ),
                  ),
                  CircularPercentIndicator(
                    radius: setWidth(40),
                    lineWidth: 4.0,
                    percent: movie.voteAverage / 10,
                    center: new Text(
                      movie.voteAverage > 0
                          ? movie.voteAverage.toString()
                          : "?",
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    progressColor: _ratingColor,
                  )
                ],
              ),
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF032541),
          title: Text('Movie Database'),
          //TODO#5:2h:build search appBar using existed events,states and repository
        ),
        body: BlocConsumer(
            cubit: _bloc,
            listener: (context, MovieListState state) {
              if (state is SuccessMovieListState) {
                _loading = false;
                _page += 1;
              } else if (state is ErrorMovieListState) {
                return showDialog<void>(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text('Error'),
                      content: SingleChildScrollView(
                        child: ListBody(
                          children: <Widget>[
                            Text(state.errorMessage),
                          ],
                        ),
                      ),
                      actions: <Widget>[
                        TextButton(
                          child: Text('OK'),
                          onPressed: () {
                            Future.delayed(Duration(seconds: 10), () {
                              _bloc.add(FetchNextPage(_page));
                            });
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              }
            },
            builder: (context, MovieListState state) {
              if (state.movies.isEmpty) {
                return Center(child: CircularProgressIndicator());
              }
              return NotificationListener(
                  onNotification: _handleScrollNotification,
                  child: ListView.builder(
                      controller: _scrollController,
                      itemCount: _calculateListItemCount(state),
                      itemBuilder: (context, index) {
                        return index < state.movies.length
                            ? _buildMovieCard(state.movies[index])
                            : Center(child: CircularProgressIndicator());
                      }));
            }));
  }
}
