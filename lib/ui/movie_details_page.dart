import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:movie_database/model/movie.dart';
import 'package:movie_database/util/view_util.dart';

class MovieDetailsPage extends StatefulWidget {
  final Movie movie;

  MovieDetailsPage(this.movie);

  @override
  _MovieDetailsPageState createState() => _MovieDetailsPageState();
}

class _MovieDetailsPageState extends State<MovieDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0xFF032541), title: Text(widget.movie.title)),
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(children: [
          Hero(
            tag: widget.movie.originalPosterPath,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: CachedNetworkImage(
                imageUrl: widget.movie.originalPosterPath,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                left: setWidth(8),
                right: setWidth(8),
                top: setWidth(10),
                bottom: setWidth(60)),
            child: Text(widget.movie.overview,
                style: TextStyle(fontSize: setSp(16))),
          )
        ]),
      ),
    );
  }
}
