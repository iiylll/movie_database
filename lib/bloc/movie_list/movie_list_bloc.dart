import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_database/bloc/movie_list/movie_list_event.dart';
import 'package:movie_database/bloc/movie_list/movie_list_state.dart';
import 'package:movie_database/service/repository/movie_repository.dart';
import 'package:movie_database/service/use_case/get_movies_with_genres_use_case.dart';

class MovieListBloc extends Bloc<MovieListEvent, MovieListState> {
  final GetMoviesWithGenresUseCase _getMoviesWithGenresUseCase;
  MovieListBloc(this._getMoviesWithGenresUseCase)
      : super(InitialMovieListState());

  @override
  Stream<MovieListState> mapEventToState(MovieListEvent event) async* {
    if (event is FetchNextPage) {
      try {
        var movies =
            await _getMoviesWithGenresUseCase.createRequest(event.page);
        yield SuccessMovieListState(state.movies + movies);
      } on NoNextPageException catch (_) {
        yield AllMoviesAreLoaded(state.movies);
      } on Exception catch (e) {
        yield ErrorMovieListState(state.movies, e.toString());
      }
    } else if (event is SearchMovieListEvent) {}
  }
}
