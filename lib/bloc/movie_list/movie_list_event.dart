import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class MovieListEvent extends Equatable {
  MovieListEvent();
}

class SearchMovieListEvent extends MovieListEvent {
  final String query;
  final int page;

  SearchMovieListEvent(this.query, this.page);

  @override
  List<Object> get props => [query, page];
}

class FetchNextPage extends MovieListEvent {
  final int page;

  FetchNextPage(this.page);

  @override
  List<Object> get props => [page];
}
