import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:movie_database/model/movie.dart';

@immutable
abstract class MovieListState extends Equatable {
  final List<Movie> movies;
  final bool hasReachEndOfResults;
  MovieListState(this.movies, this.hasReachEndOfResults);

  @override
  List<Object> get props => [movies, hasReachEndOfResults];
}

class InitialMovieListState extends MovieListState {
  InitialMovieListState() : super([], false);
}

class SuccessMovieListState extends MovieListState {
  SuccessMovieListState(movies) : super(movies, false);
}

class ErrorMovieListState extends MovieListState {
  final String errorMessage;
  ErrorMovieListState(movies, this.errorMessage) : super(movies, false);
}

class AllMoviesAreLoaded extends MovieListState {
  AllMoviesAreLoaded(movies) : super(movies, true);
}
