# Movie Database project

## Important info

* Didn't use `json_serializable` library, because data models are small and there is no serialization to store data, so there will be more pain than gain :-)

* I am used to TODO-driven development, so will create some TODO tickets in the project with the assumptions it will continue developing. You can found them by substring `TODO#`

* Used my own injector.dart as DI container without adding extra dependency, where added for example interceptor for Dio to log all http requests/responses

* Api key has been stored in the repository to let you bootstrap project fast, more secure option that came to my mind was to store it in a text file, share separately and include some instruction into README. 

* In the list page I am loading images with original size for animation reason, but also have the possibility to work with smaller images.

* While testing Error states in UI manually, found and fixed an error when user will be disconnected from Internet and reconnected again.

### Dependencies:
* flutter_bloc (for state management). Using 6.06 version and there is breaking change compared to version 5 (`bloc` parameter renamed to `cubit`)
* equatable (support library for flutter_bloc, used to optimize and avoid if the state doesn't change)
* dio (for http requests)
* cached_network_image ( to cache images )
* flutter_screenutil ( Good lightweight lib for adaptive design (4'' vs 7'' smartphones have big diff) )
* percent_indicator ( for rating of the movie )

#### Dio cache parameters: 
* < 1 day: Return data from cache directly (irrelevant with network).

* 1-5 days:
  1. Get data from network first.
  2. If getting data from network succeeds, refresh cache.
  3. If getting data from network fails or no network avaliable, try get data from cache instead of an error.
* \> 3 days : It won't use cache anymore, and the cache will be deleted at the right time.

### Dev dependencies:
*pedantic* for static analysis and lints
*bloc_test* and *mockito* for mocking objects in unit tests for bloc


### How to
#### `flutter test`
#### `flutter run`

### Media
#### Video

##### From real device : iPad mini gen. 5
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/MDS1xlg2Td0/0.jpg)](https://www.youtube.com/watch?v=MDS1xlg2Td0)

##### From virtual device iPhone 11 Pro Max
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/hporKBal478/0.jpg)](https://www.youtube.com/watch?v=hporKBal478)


#### Screenshots

![Screenshot](preview_image.png)
