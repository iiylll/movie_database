import 'package:flutter_test/flutter_test.dart';
import 'package:movie_database/model/movie.dart';

void main() {
  group('popular movie model', () {
    test('deserializing with all values', () async {
      var json = {
        'overview': 'custom overview',
        'title': 'test title',
        'vote_average': 0,
        'poster_path': 'url'
      };
      var movie = Movie.fromJson(json);
      expect(movie.voteAverage, isNotNull);
      expect(movie.title, isNotNull);
      expect(movie.posterPath, isNotNull);
      expect(movie.overview, isNotNull);
    });

    test('deserializing with missing values', () async {
      Map<String, dynamic> json = {};
      var movie = Movie.fromJson(json);
      expect(movie.voteAverage, isNull);
      expect(movie.title, isNull);
      expect(movie.posterPath, isNull);
      expect(movie.overview, isNull);
    });
  });
}
