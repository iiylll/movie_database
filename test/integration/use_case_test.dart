import 'package:flutter_test/flutter_test.dart';
import 'package:movie_database/di/injector.dart';
import 'package:movie_database/service/use_case/get_movies_with_genres_use_case.dart';

void main() {
  GetMoviesWithGenresUseCase _getMoviesWithGenresUseCase;

  setUp(() {
    Injector.configure(Flavor.PRODUCTION);
    _getMoviesWithGenresUseCase = Injector().getMoviesWithGenresUseCase;
  });

  group('use case integration tests', () {
    test('getMoviesWithGenresUseCase', () async {
      var moviesWithGenres = await _getMoviesWithGenresUseCase.createRequest(1);
      expect(moviesWithGenres.length, isPositive);
      expect(moviesWithGenres[0].genres[0].name, isNotNull);
    });
  });
}
