import 'package:flutter_test/flutter_test.dart';
import 'package:movie_database/di/injector.dart';
import 'package:movie_database/service/repository/movie_repository.dart';

void main() {
  MovieRepository _movieRepository;
  setUp(() {
    Injector.configure(Flavor.PRODUCTION);
    _movieRepository = Injector().movieRepository;
  });
  group('movie repository integration test', () {
    test('getting popular', () async {
      var popularMovies = await _movieRepository.getPopularMovies(1);
      expect(popularMovies.length, RESULT_PER_PAGE);
    });

    test('search', () async {
      var jumanjiMovies = await _movieRepository.search("Jumanji", 1);
      expect(jumanjiMovies.length, isPositive);

      var avengerMovies = await _movieRepository.search("Avenger", 1);
      expect(avengerMovies.length, isPositive);

      expect(jumanjiMovies, equals(jumanjiMovies)); // just to be sure :-)
      expect(jumanjiMovies, isNot(avengerMovies));
    });
  });
}
