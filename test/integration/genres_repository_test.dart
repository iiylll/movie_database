import 'package:flutter_test/flutter_test.dart';
import 'package:movie_database/di/injector.dart';
import 'package:movie_database/service/repository/genres_repository.dart';

void main() {
  GenresRepository _genresRepository;
  setUp(() {
    Injector.configure(Flavor.PRODUCTION);
    _genresRepository = Injector().genresRepository;
  });

  group('genres repository integration test', () {
    test('genre', () async {
      var genres = await _genresRepository.getGenres();
      expect(genres.length, isPositive);
    });
  });
}
