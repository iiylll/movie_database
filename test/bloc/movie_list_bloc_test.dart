import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:movie_database/bloc/movie_list/movie_list_bloc.dart';
import 'package:movie_database/bloc/movie_list/movie_list_event.dart';
import 'package:movie_database/bloc/movie_list/movie_list_state.dart';
import 'package:movie_database/constant/service_constant.dart';
import 'package:movie_database/model/movie.dart';
import 'package:movie_database/service/repository/movie_repository.dart';
import 'package:movie_database/service/use_case/get_movies_with_genres_use_case.dart';

class MockGetMoviesWithGenresUseCase extends Mock
    implements GetMoviesWithGenresUseCase {}

main() {
  MockGetMoviesWithGenresUseCase _mockUseCase;
  MovieListBloc _movieListBloc;

  setUp(() {
    _mockUseCase = MockGetMoviesWithGenresUseCase();
    _movieListBloc = MovieListBloc(_mockUseCase);
  });

  group('mobie list bloc', () {
    test('initial state test', () {
      expect(_movieListBloc.state, InitialMovieListState());
    });

    blocTest(
      'emits [] when nothing is added',
      build: () => _movieListBloc,
      expect: [],
    );

    blocTest(
      'emits FetchNextPage and expect 5 movies',
      build: () {
        var length = 5;
        var movies = List.generate(
            length, (index) => Movie.fromJson(Map<String, dynamic>()));

        when(_mockUseCase.createRequest(any)).thenAnswer((_) async => movies);
        return _movieListBloc;
      },
      act: (bloc) => bloc.add(FetchNextPage(1)),
      expect: [
        SuccessMovieListState(
            List.generate(5, (index) => Movie.fromJson(Map<String, dynamic>())))
      ],
    );

    blocTest(
      'emits FetchNextPage with server error',
      build: () {
        when(_mockUseCase.createRequest(any))
            .thenThrow(Exception(UNKNOWN_ERROR_MESSAGE));
        return _movieListBloc;
      },
      act: (bloc) => bloc.add(FetchNextPage(1)),
      expect: [ErrorMovieListState(List<Movie>(), UNKNOWN_ERROR_MESSAGE)],
    );

    blocTest(
      'emits FetchNextPage with no more movies',
      build: () {
        when(_mockUseCase.createRequest(any)).thenThrow(NoNextPageException());
        return _movieListBloc;
      },
      act: (bloc) => bloc.add(FetchNextPage(1)),
      expect: [AllMoviesAreLoaded(List<Movie>())],
    );
  });
}
